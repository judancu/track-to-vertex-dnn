import track_to_vertex_dNN_hyperparam
import track_to_vertex_dNN
import numpy as np
import util
import algos
import pandas as pd

events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-zmm-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-938relval.root')

#track_features = ['pt', 'z0', 'eta', 'chi2', 'tanL', 'dz']
track_features = ['pt', 'eta', 'chi2', 'dz']
truth_label = ['fromPV']
featureNumber = len(track_features)

alltracks = pd.concat(events, ignore_index=True)

track_avg = float(len(alltracks))/float(len(events))
track_avg = round(track_avg)

X = alltracks[alltracks['pt']<=500][track_features]
X['eta'] = abs(X['eta'])
X['dz'] = abs(X['dz'])
y = alltracks[alltracks['pt']<=500][truth_label]

batch_size = 400
is_z0 = False
is_tanL = False

title = 'Dense_pt500_4feat_Zmumu'

track_to_vertex_dNN.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
#track_to_vertex_dNN_hyperparam.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
