import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
import sklearn.metrics
sys.path.append("%s/../lib" % os.getcwd())
import util
import algos
import tensorflow as tf
import os
import matplotlib.colors as col
import scipy.interpolate
import pickle

from keras import backend as K
from keras.layers import Dense, Dropout
from keras import regularizers
from keras.models import Sequential
from keras import optimizers


def dNN_architecture(inputFeatures, node, depth, dropout):
    dnn = keras.layers.Dense(node, activation='relu')(inputFeatures)
    dnn = keras.layers.Dropout(dropout)(dnn)
    if depth>1:
        for i in range(depth-1):
            dnn = keras.layers.Dense(node, activation='relu')(dnn)
            dnn = keras.layers.Dropout(dropout)(dnn)
    return dnn

def dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber, params):

    #nodes = [8, 16, 32, 64]
    #depths = [1, 2, 4, 8]

    node = params['node'][0]
    depth = params['depth'][0]

    for lr in params['lr']:
        for dropout in params['dropout']:

            print('Learning rate: ' + str(lr) + ', Dropout: ' + str(dropout))

            model_dNN = Sequential()

            inputFeatures = keras.layers.Input(shape=(featureNumber,))
            dnn = dNN_architecture(inputFeatures, node, depth, dropout)
            predictFraction = keras.layers.Dense(1, activation='sigmoid')(dnn)

            model_dNN = keras.models.Model(inputs=[inputFeatures],outputs=[predictFraction])

            rmsprop = optimizers.RMSprop(lr=lr)
            model_dNN.compile(loss='binary_crossentropy', optimizer=rmsprop, metrics=['accuracy'])

            batch_size1 = track_avg * batch_size

            X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.1, random_state=42)

            history = model_dNN.fit(X_train, y_train, validation_split=0.33, epochs=20, batch_size=batch_size1)

            plt.plot(history.history['val_acc'])
            plt.plot(history.history['acc'])
            plt.title('Model accuracy (' + str(title) + ', ' + str(dropout) + ' dropout, ' + str(lr) + ' learning rate)')
            plt.ylabel('accuracy')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='lower right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_accuracy_'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.png')
            plt.clf()

            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('Model loss (' + str(title) + ', ' + str(dropout) + ' dropout, ' + str(lr) + ' learning rate)')
            plt.ylabel('loss')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='upper right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_loss_'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.png')
            plt.clf()

            probPV = model_dNN.predict(X_test)

            effis, puris = [], [] # efficiency, purity
            for cut in np.arange(0., 1., 0.02):
                e, p = util.matchPerf(y_test, y_test[probPV > cut])
                effis.append(e)
                puris.append(p)

            plt.plot(effis, puris, '.')
            plt.xlabel("Matching Efficiency")
            plt.ylabel("Purity")
            plt.title('Efficiency vs Purity ROC for association (' + str(title) + ', ' + str(dropout) + ' dropout, ' + str(lr) + ' learning rate)')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/EffvsPur_'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.png')
            plt.clf()

            AUC = sklearn.metrics.auc(effis, puris)

            file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.txt', "w")
            file.write('AUC of purity vs efficiency: ' + str(AUC))
            file.write("\n")
            auc1 = AUC

            pur = scipy.interpolate.interp1d(effis, puris)
            pur1 = float(pur(0.95))
            file.write('Purity at which Efficiency =95%:  ' + str(pur1))
            file.write("\n")

            classes = model_dNN.predict(X_test, batch_size=128)

            plt.hist(classes[:,0][y_test['fromPV']==1], range=[0., 1.], bins=50, alpha=0.5, density = True)
            plt.hist(classes[:,0][y_test['fromPV']==0], range=[0., 1.], bins=50, alpha=0.5, density = True)
            plt.xlabel("predicted weights")#, horizontalalignment='right', x=1.0)
            plt.title('Network output (' + str(title) + ', ' + str(dropout) + ' dropout, ' + str(lr) + ' learning rate)')
            plt.legend(['PV', 'PU'], loc='upper right')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/PVvsPU_'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.png')
            plt.clf()

            FPR_dNN = []
            TPR_dNN = []

            for cut in np.arange(0., 1., 0.01):
                N = len(y_test) - sum(y_test['fromPV'])
                P = sum(y_test['fromPV'])
                FP = np.sum((classes[:,0] > cut) & (y_test['fromPV'] == 0))
                TP = np.sum((classes[:,0] > cut) & (y_test['fromPV'] == 1))
                if (N != 0):
                    FPR_dNN.append(float(FP)/float(N))
                    TPR_dNN.append(float(TP)/float(P))

            AUC = sklearn.metrics.auc(TPR_dNN, FPR_dNN)
            file.write('AUC for traditional ROC curve: ' + str(AUC))
            auc2 = AUC
            file.write("\n")

            fpr = scipy.interpolate.interp1d(FPR_dNN[::-1], TPR_dNN[::-1])
            fpr_DNN1 = float(fpr(0.01))
            file.write('TPR at which FPR = 1%:  ' + str(fpr_DNN1))
            file.write("\n")

            plt.semilogy(TPR_dNN, FPR_dNN)
            plt.hlines(1e-2, 0, fpr_DNN1, linestyle="dashed")
            plt.vlines(fpr_DNN1, 1e-4, 1e-2, linestyle="dashed")
            plt.xlabel("True Positive Rate")
            plt.ylabel("False Positive Rate")
            plt.title('TPR vs FPR ROC for association (' + str(title) + ', ' + str(dropout) + ' dropout, ' + str(lr) + ' learning rate)')
            plt.savefig('/home/hep/jd918/project/L1_trigger/plots/FPR_vs_TPR_'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.png')
            plt.clf()

            file.close()

            dumplings = [auc1, auc2, fpr_DNN1, pur1, history.history['val_loss'][-1]]
            fileName = '/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'_drop'+str(dropout)+'_lr'+str(lr)+'.pkl'
            fileObject = open(fileName, 'wb')
            pickle.dump(dumplings, fileObject)
            pickle.dump(TPR_dNN, fileObject)
            pickle.dump(FPR_dNN, fileObject)
            pickle.dump(effis, fileObject)
            pickle.dump(puris, fileObject)
            fileObject.close()
