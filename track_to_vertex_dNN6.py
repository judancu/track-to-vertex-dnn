import track_to_vertex_dNN_hyperparam
import track_to_vertex_dNN_hyperparam1
import track_to_vertex_dNN
import numpy as np
import util
import algos
import pandas as pd

events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-938relval.root')

#track_features = ['pt', 'z0', 'eta', 'chi2', 'tanL', 'dz']
track_features = ['pt', 'z0', 'eta', 'chi2', 'dz']
truth_label = ['fromPV']
featureNumber = len(track_features)

alltracks = pd.concat(events, ignore_index=True)

track_avg = float(len(alltracks))/float(len(events))
track_avg = round(track_avg)

X = alltracks[alltracks['pt']<=500][track_features]
X['eta'] = abs(X['eta'])
X['dz'] = abs(X['dz'])
X['pt'] = 1.0/X['pt']
y = alltracks[alltracks['pt']<=500][truth_label]

batch_size = 400
is_z0 = True
is_tanL = False

title = 'Dense_pt500_5feat_invpt'

params = {'lr': [0.03, 0.1],#[0.0001, 0.0003, 0.001, 0.003, 0.01],
 'node':[32],
 'depth': [2],
 'dropout': [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]}#(0, 0.40, 10)}

#track_to_vertex_dNN.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
#track_to_vertex_dNN_hyperparam.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
track_to_vertex_dNN_hyperparam1.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber, params)
