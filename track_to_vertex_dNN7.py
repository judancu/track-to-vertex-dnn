import track_to_vertex_dNN
import track_to_vertex_dNN_hyperparam
import numpy as np
import util
import algos
import pandas as pd

events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-zmm-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-938relval.root')

track_features = ['pt', 'z0', 'eta', 'chi2', 'tanL', 'dz']
truth_label = ['fromPV']
featureNumber = len(track_features)

alltracks = pd.concat(events, ignore_index=True)

track_avg = float(len(alltracks))/float(len(events))
track_avg = round(track_avg)

X = alltracks[track_features]
y = alltracks[truth_label]

batch_size = 400
is_z0 = True
is_tanL = True

title = 'Dense_basic_Zmumu'

track_to_vertex_dNN.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
#track_to_vertex_dNN_hyperparam.dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
