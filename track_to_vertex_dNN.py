import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
import sklearn.metrics
sys.path.append("%s/../lib" % os.getcwd())
import util
import algos
import tensorflow as tf
import os
import matplotlib.colors as col
import scipy.interpolate
import pickle

from keras import backend as K
from keras.layers import Dense, Dropout
from keras import regularizers
from keras.models import Sequential
from keras import optimizers


def dNN(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber):

    model_dNN = Sequential()

    inputFeatures = keras.layers.Input(shape=(featureNumber,))
    dnn = keras.layers.Dense(32, activation='relu')(inputFeatures)
    dnn = keras.layers.Dropout(0.1)(dnn)
    dnn = keras.layers.Dense(32, activation='relu')(dnn)
    dnn = keras.layers.Dropout(0.1)(dnn)
    predictFraction = keras.layers.Dense(1, activation='sigmoid')(dnn)

    model_dNN = keras.models.Model(inputs=[inputFeatures],outputs=[predictFraction])

    rmsprop = optimizers.RMSprop(lr=0.01)
    model_dNN.compile(loss='binary_crossentropy', optimizer=rmsprop, metrics=['accuracy'])

    batch_size1 = track_avg * batch_size

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.1, random_state=42)

    history = model_dNN.fit(X_train, y_train, validation_split=0.33, epochs=20, batch_size=batch_size1)

    plt.plot(history.history['val_acc'], label="val")
    plt.plot(history.history['acc'], label="train")
    plt.title('Model accuracy (' + str(title) + ')')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(loc='upper right')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_accuracy_'+str(title)+'.png')
    plt.clf()

    plt.plot(history.history['loss'], label="train")
    plt.plot(history.history['val_loss'], label="val")
    plt.title('Model loss (' + str(title) + ')')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(loc='upper right')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_loss_'+str(title)+'.png')
    plt.clf()

    probPV = model_dNN.predict(X_test)

    effis, puris = [], [] # efficiency, purity
    for cut in np.arange(0., 1., 0.02):
        e, p = util.matchPerf(y_test, y_test[probPV > cut])
        effis.append(e)
        puris.append(p)

    plt.plot(effis, puris, '.')
    plt.xlabel("Matching Efficiency")
    plt.ylabel("Purity")
    plt.title('Efficiency vs Purity ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/EffvsPur_'+str(title)+'.png')
    plt.clf()

    AUC = sklearn.metrics.auc(effis, puris)

    file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.txt', "w")
    file.write('AUC of purity vs efficiency: ' + str(AUC))
    file.write("\n")
    auc1 = AUC

    pur = scipy.interpolate.interp1d(effis, puris)
    file.write('Purity at which Efficiency =95%:  ' + str(pur(0.95)))
    pur1 = float(pur(0.95))
    file.write("\n")

    classes = model_dNN.predict(X_test, batch_size=128)

    plt.hist2d(classes[:,0], X_test['pt'], range=[[0.0, 1.], [0, 1000]], bins=50, norm=col.LogNorm());
    plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$p_t$ (GeV)")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $p_t$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvspt_'+str(title)+'.png')
    plt.clf()

    if is_z0:
        plt.hist2d(classes[:,0], X_test['z0'], range=[[0., 1.], [-15, 15]], bins=50, norm=col.LogNorm());
        plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
        plt.ylabel("$z0$ (cm)")#, horizontalalignment='right', y=1.0)
        plt.title('Correlation plot between the network output and $z_0$ (' + str(title) + ')')
        plt.colorbar()
        plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvsz0_'+str(title)+'.png')
        plt.clf()

    plt.hist2d(classes[:,0], X_test['dz'], range=[[0., 1.], [-30, 30]], bins=50, norm=col.LogNorm());
    plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("dz (cm)")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and dz (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvsdz_'+str(title)+'.png')
    plt.clf()

    plt.hist2d(classes[:,0], X_test['chi2'], range=[[0., 1.], [0, 350]], bins=50, norm=col.LogNorm());
    plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$\chi^2$")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $\chi^2$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvschi2_'+str(title)+'.png')
    plt.clf()

    plt.hist2d(classes[:,0], X_test['eta'], range=[[0., 1.], [-2.5, 2.5]], bins=50, norm=col.LogNorm());
    plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$\eta$")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $\eta$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvseta_'+str(title)+'.png')
    plt.clf()

    if is_tanL:
        plt.hist2d(classes[:,0], X_test['tanL'], range=[[0., 1.], [0, 6]], bins=50, norm=col.LogNorm());
        plt.xlabel("network output")#, horizontalalignment='right', x=1.0)
        plt.ylabel("tan $\lambda$")#, horizontalalignment='right', y=1.0)
        plt.title('Correlation plot between the network output and tan $\lambda$ (' + str(title) + ')')
        plt.colorbar()
        plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NNvstanL_'+str(title)+'.png')
        plt.clf()

    plt.hist(classes[:,0][y_test['fromPV']==1], range=[0., 1.], bins=50, alpha=0.5, density = True)
    plt.hist(classes[:,0][y_test['fromPV']==0], range=[0., 1.], bins=50, alpha=0.5, density = True)
    plt.xlabel("predicted weights")#, horizontalalignment='right', x=1.0)
    plt.title('Network output (' + str(title) + ')')
    plt.legend(['PV', 'PU'], loc='upper right')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/PVvsPU_'+str(title)+'.png')
    plt.clf()

    FPR_dNN = []
    TPR_dNN = []

    for cut in np.arange(0., 1., 0.01):
        N = len(y_test) - sum(y_test['fromPV'])
        P = sum(y_test['fromPV'])
        FP = np.sum((classes[:,0] > cut) & (y_test['fromPV'] == 0))
        TP = np.sum((classes[:,0] > cut) & (y_test['fromPV'] == 1))
        if (N != 0):
            FPR_dNN.append(float(FP)/float(N))
            TPR_dNN.append(float(TP)/float(P))

    AUC = sklearn.metrics.auc(TPR_dNN, FPR_dNN)
    file.write('AUC for traditional ROC curve: ' + str(AUC))
    auc2 = AUC
    file.write("\n")

    fpr = scipy.interpolate.interp1d(FPR_dNN[::-1], TPR_dNN[::-1])
    fpr_DNN1 = float(fpr(0.01))
    file.write('TPR at which FPR = 1%:  ' + str(fpr_DNN1))
    file.write("\n")

    plt.semilogy(TPR_dNN, FPR_dNN)
    #plt.axvline(x = fpr_DNN1, ymin = e-4, ymax = e-2, linestyle="dashed")
    #plt.axhline(y = e-2, xmin = 0, xmax = fpr_DNN1, linestyle="dashed")
    #plt.axvline(x = fpr_DNN1, linestyle="dashed")
    #plt.axhline(y = 1e-2, linestyle="dashed")
    plt.hlines(1e-2, 0, fpr_DNN1, linestyle="dashed")
    plt.vlines(fpr_DNN1, 1e-4, 1e-2, linestyle="dashed")
    #plt.semilogy(TPR_dNN, FPR_dNN)
    plt.xlabel("True Positive Rate")
    plt.ylabel("False Positive Rate")
    plt.title('TPR vs FPR ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/FPR_vs_TPR_'+str(title)+'.png')
    plt.clf()

    file.close()

    dumplings = [auc1, auc2, fpr_DNN1, pur1, history.history['val_loss'][-1]]
    fileName = '/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.pkl'
    fileObject = open(fileName, 'wb')
    pickle.dump(dumplings, fileObject)
    pickle.dump(TPR_dNN, fileObject)
    pickle.dump(FPR_dNN, fileObject)
    pickle.dump(effis, fileObject)
    pickle.dump(puris, fileObject)
    fileObject.close()
